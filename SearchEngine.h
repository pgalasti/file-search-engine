#pragma once
#include "stdafx.h"
#include<queue>
#include<list>
#include<future>
#include<mutex>

#include "Directory.h"
#ifdef _DEBUG
#include "Logging.h"
using namespace GUtils::Debug;
#endif

using namespace FileSearch::Structures;

namespace FileSearch {
	namespace Engine {

#define MAX_TOKEN_LENGTH 512

		struct DllExport SearchRecord
		{
			void Initialize() { ZeroMemory(this, sizeof SearchRecord); }
			char Rootpath[_MAX_DIR];
			char Token[MAX_TOKEN_LENGTH];
			bool IgnoreCase;
		};

		class DllExport SearchEngine
		{
			DEBUG_LOG_MEMBER;

		public:

			SearchEngine();
			~SearchEngine();
			BOOL Load(const SearchRecord& record);
			std::list<File> GetFileList();
			BOOL SearchForTokenInFiles(std::list<File>* pFiles, std::list<std::string>& results);
			

		protected:

			SearchRecord m_Record;

			std::list<Directory> GetChildDirectories(Directory& parentDirectory);
			BOOL SearchForToken(File* pFile);

			std::mutex m_ResultMutex;
			void AddResult(const std::string& result);
			std::list<std::string> m_Results;

		};
	}
}
