#pragma once
#include "stdafx.h"
#include <fstream>
#include <vector>
#include <list>
#ifdef _DEBUG
#include "Logging.h"
using namespace GUtils::Debug;
#endif

namespace FileSearch {
	namespace Structures {

		class DllExport File 
		{
		public:
			File();
			File(const char* szPath);
			File(const File& other);
			~File();


			const unsigned int GetFileSize() const;
			std::string GetFileName() const;
			std::string GetFileExt() const;
			std::string GetFullPath() const;
			std::list<std::string> GetFileLines();
			std::vector<BYTE> GetFileBytes();
			void SetFilePath(const char* szPath);
			BOOL ProcessFile();
			bool IsValidFile() const;
		protected:
			std::fstream* m_pFile;
			std::string m_FilePath;
			std::string m_FullFileCharacters; 
			std::vector<BYTE> m_Bytes;
		};
	}
}
