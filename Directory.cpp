#pragma once
#include "stdafx.h"
#include "Directory.h"

using namespace FileSearch::Structures;

Directory::Directory()
{
	::Directory("");
}

Directory::Directory(const char* szPath)
{
	this->SetDirectory(szPath);
}

Directory::~Directory()
{
	m_Directories.clear();
	m_Files.clear();
}

Directory::Directory(const Directory& other)
{
	*this = other;
}

const std::string Directory::GetDirectoryName() const
{
	return m_DirectoryName;
}

const std::list<File> Directory::GetFiles() const
{
	return m_Files;
}

const std::list<Directory> Directory::GetDirectories() const
{
	return m_Directories;
}

const unsigned int Directory::GetFileCount() const
{
	return m_Files.size();
}

const unsigned int Directory::GetDirectoryCount() const
{
	return m_Directories.size();
}

BOOL Directory::CheckDirectory()
{
	BOOL result = this->Process();
	if (!result)
		return FALSE;

	return TRUE;
}

void Directory::SetDirectory(const char* szPath)
{
	m_DirectoryName = szPath;
	
	size_t endCharacterInd = strlen(szPath)-1;
	if (szPath[endCharacterInd] != '\\')
		m_DirectoryName.append("\\");

}

BOOL Directory::Process()
{
	m_Files.clear();
	m_Directories.clear();

	WIN32_FIND_DATA FFD;
	DWORD dwError = 0;
	
	char szDirectorySearch[_MAX_PATH];
	strcpy(szDirectorySearch, m_DirectoryName.c_str());
	strcat(szDirectorySearch, "*");

	HANDLE hFind = INVALID_HANDLE_VALUE;
	hFind = FindFirstFile(szDirectorySearch, &FFD);

	if (hFind == INVALID_HANDLE_VALUE)
		return FALSE;

	do
	{
		if (FFD.cFileName[0] == '.' && strlen(FFD.cFileName) < 3)
			continue;

		std::string path = m_DirectoryName;
		path.append(FFD.cFileName);

		if (FFD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			m_Directories.push_back(Directory(path.c_str()));
		}
		else
		{
			File file(path.c_str());
			m_Files.push_back(file);
		}

	} while (FindNextFile(hFind, &FFD) != FALSE);

	dwError = GetLastError();
	FindClose(hFind);

	return TRUE;
}

Directory& Directory::operator = (const Directory &other)
{
	this->m_DirectoryName = other.m_DirectoryName;
	this->m_Directories = other.m_Directories;
	this->m_Files = other.m_Files;

	return *this;
}
