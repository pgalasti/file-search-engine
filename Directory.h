#pragma once
#include "stdafx.h"
#include "File.h"

#ifdef _DEBUG
#include "Logging.h"
using namespace GUtils::Debug;
#endif

namespace FileSearch {
	namespace Structures {

		class DllExport Directory
		{

		public:
			Directory();
			Directory(const char* szPath);
			Directory(const Directory& other);
			~Directory();
			
			const std::string GetDirectoryName() const;
			const std::list<File> GetFiles() const;
			const std::list<Directory> GetDirectories() const;
			const unsigned int GetFileCount() const;
			const unsigned int GetDirectoryCount() const;

			BOOL CheckDirectory();
			void SetDirectory(const char* szPath);

			Directory& operator=(const Directory &other);

		protected:

			std::string m_DirectoryName;
			std::list<File> m_Files;
			std::list<Directory> m_Directories;

			BOOL Process();
		};


	}
}
