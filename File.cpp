#pragma once
#include "stdafx.h"
#include "File.h"

#include<string>

//#include<sys/stat.h>

using namespace FileSearch::Structures;

File::File()
{
	
}

File::File(const char* szPath)
{
	m_pFile = new std::fstream();
	this->SetFilePath(szPath);
}

File::File(const File& other)
{
	m_pFile = new std::fstream();
	this->SetFilePath(other.m_FilePath.c_str());
}

File::~File()
{
	if (m_pFile->is_open())
		m_pFile->close();

	SAFE_DELETE(m_pFile);
	
}

void File::SetFilePath(const char* szPath)
{
	m_FilePath = szPath;
}

const unsigned int File::GetFileSize() const
{
	HANDLE hFile = CreateFile(m_FilePath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	LARGE_INTEGER byteSize;
	ZeroMemory(&byteSize, sizeof LARGE_INTEGER);

	if (!GetFileSizeEx(hFile, &byteSize))
		return 0;

	return byteSize.LowPart;
}

std::string File::GetFileName() const
{
	const unsigned int START_POS = m_FilePath.find_last_of("\\")+1;
	const unsigned int FULL_LENGTH = m_FilePath.length();

	const std::string NAME = m_FilePath.substr(START_POS, FULL_LENGTH - START_POS);
	return NAME;
}

std::string File::GetFileExt() const
{
	const unsigned int START_POS = m_FilePath.find_last_of(".") + 1;
	const unsigned int FULL_LENGTH = m_FilePath.length();

	const std::string EXT = m_FilePath.substr(START_POS, FULL_LENGTH - START_POS);
	return EXT;
}

std::string File::GetFullPath() const
{
	return m_FilePath;
}

std::list<std::string> File::GetFileLines()
{
	std::list<std::string> lines;
	if (!m_pFile->is_open())
		return lines;


	return lines;
}

BOOL File::ProcessFile()
{
	if (m_pFile->is_open())
		m_pFile->close();

	m_pFile->open(m_FilePath);
	if (!m_pFile->is_open())
		return FALSE;

	std::string line;
	while (std::getline(*m_pFile, line))
		m_FullFileCharacters.append(line);

	m_pFile->close();
	return TRUE;
}

std::vector<BYTE> File::GetFileBytes()
{
	const size_t SIZE = m_FullFileCharacters.length();

	m_Bytes.clear();
	m_Bytes.resize(SIZE);
	for (int i = 0; i < SIZE; i++)
		m_Bytes[i] = m_FullFileCharacters[i];
	return m_Bytes;
}

bool File::IsValidFile() const
{
	//struct stat buffer;
	//if (stat(m_FilePath.c_str(), &buffer) != -1)
	//	return true;

	//return false;

	std::ifstream file(m_FilePath.c_str());
	return file.good();
}
