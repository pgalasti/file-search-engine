#pragma once
#include "stdafx.h"
#include "SearchEngine.h"
#include "Directory.h"

using namespace FileSearch::Engine;
using namespace FileSearch::Structures;

SearchEngine::SearchEngine()
{
	DEBUG_CREATE_LOG("SearchEngine");
}

SearchEngine::~SearchEngine()
{
	DEBUG_MESSAGE("SearchEngine Deconstructor");
	m_Results.clear();

	DEBUG_DESTROY_LOG
}

BOOL SearchEngine::Load(const SearchRecord& record)
{
	DEBUG_MESSAGE("Loading Search Criteria");
	
	if (strlen(record.Rootpath) == 0)
	{
		DEBUG_MESSAGE("Root Directory is blank.");
		return FALSE;
	}

	if (strlen(record.Token) == 0)
	{
		DEBUG_MESSAGE("Token is blank.");
		return FALSE;
	}

	memcpy(&m_Record, &record, sizeof SearchRecord);

	return TRUE;
}

std::list<File> SearchEngine::GetFileList()
{
	DEBUG_MESSAGE("Starting file gather process...");

	const std::string ROOT_PATH = m_Record.Rootpath;
	const std::string SEARCH_TOKEN = m_Record.Token;
	const bool IGNORE_CASE = m_Record.IgnoreCase;

	Directory directory(ROOT_PATH.c_str());
	std::list<File> allFiles;

	if (directory.CheckDirectory())
	{
		// Get all child directories
		std::list<Directory> allDirectories = this->GetChildDirectories(directory); 

		// Get all files from root directory
		for (auto file : directory.GetFiles())
			allFiles.push_back(file);

		// Get all files from child directories
		for (auto directory : allDirectories)
		{
			if (directory.CheckDirectory())
			{
				for (auto file : directory.GetFiles())
					allFiles.push_back(file);
			}
		}

#ifdef _DEBUG
		const std::string DEBUG_MESSAGE_DIRS = "Directories found: " + std::string(std::to_string((unsigned int)allDirectories.size()));
		const std::string DEBUG_MESSAGE_FILES = "Files found: " + std::string(std::to_string((unsigned int)allFiles.size()));

		DEBUG_MESSAGE(DEBUG_MESSAGE_DIRS);
		DEBUG_MESSAGE(DEBUG_MESSAGE_FILES);
#endif
	}
	
	DEBUG_MESSAGE("Ending file gather process.");
	return allFiles;
}

std::list<Directory> SearchEngine::GetChildDirectories(Directory& parentDirectory)
{
	std::list<Directory> embeddedDirectories;
	if (parentDirectory.CheckDirectory())
	{
		embeddedDirectories = parentDirectory.GetDirectories();
		
		std::list<Directory> addedDirectories;
		for (auto directory : embeddedDirectories)
			addedDirectories = this->GetChildDirectories(directory);
		
		for (auto directory : addedDirectories)
			embeddedDirectories.push_back(directory);
	}

	return embeddedDirectories;
}

BOOL SearchEngine::SearchForTokenInFiles(std::list<File>* pFiles, std::list<std::string>& results)
{
	DEBUG_MESSAGE("Starting token search process...");

	std::list<std::future<BOOL>> futures;
	std::list<File*> sustainedFiles;
	auto RemoveSustainedFiles = [&sustainedFiles] {
		for (auto iter = sustainedFiles.begin(); iter != sustainedFiles.end(); iter++)
			delete (*iter);
	};

	for (auto file : *pFiles)
	{
		File* f = new File(file);
		sustainedFiles.push_back(f);
	}
	for (auto file : sustainedFiles)
		futures.push_back(std::async(&SearchEngine::SearchForToken, this, std::move(file)));

	bool hasFailed = false;
	for (auto futureIter = futures.begin(); futureIter != futures.end(); futureIter++)
	{
		if (!futureIter->get())
			hasFailed = true;
	}

	RemoveSustainedFiles();

	if (hasFailed)
	{
		ERROR_MESSAGE("Failure in a future get()");
		return FALSE;
	}
	results = m_Results;

	DEBUG_MESSAGE("Ending token search process...");
	return TRUE;
}

BOOL SearchEngine::SearchForToken(File* pFile)
{
	if (!pFile->ProcessFile())
	{
		DEBUG_MESSAGE(std::string("Failure to process the file: ") + std::string(pFile->GetFileName()));
		return TRUE;
	}

	const unsigned int MIN_MAX_RESULT = 20;
	const char* TOKEN = m_Record.Token;
	const unsigned int TOKEN_SIZE = strlen(m_Record.Token);
	std::vector<BYTE> fileBytes = pFile->GetFileBytes();


	unsigned int remainingBytes = fileBytes.size();
	for (unsigned int i = 0; i < fileBytes.size(); i++)
	{
		remainingBytes = fileBytes.size() - i;
		if (TOKEN_SIZE > remainingBytes)
			return TRUE;

		bool isMatch = true;
		for (int j = 0; j < TOKEN_SIZE; j++)
		{
			if (m_Record.IgnoreCase)
			{
				if (toupper(TOKEN[j]) != toupper(fileBytes[i + j]))
				{
					isMatch = false;
					break;
				}
			}
			else
			{
				if (TOKEN[j] != fileBytes[i + j])
				{
					isMatch = false;
					break;
				}
			}
		}

		if (isMatch)
		{
			int resultStartIndex = i - MIN_MAX_RESULT;
			int resultEndIndex = i + MIN_MAX_RESULT + TOKEN_SIZE;
			if (i < MIN_MAX_RESULT)
			{
				resultStartIndex = 0; 
			}
			if (i + MIN_MAX_RESULT > fileBytes.size())
			{
				resultEndIndex = fileBytes.size();
			}

			std::string fileLine;
			for (int i = resultStartIndex; i < resultEndIndex; i++)
				fileLine += fileBytes[i];

			std::string result = std::string(pFile->GetFileName()) + std::string(":\t") + fileLine;
			this->AddResult(result);

			return TRUE;
		}
	}

	return TRUE;
}

void SearchEngine::AddResult(const std::string& result)
{
	std::lock_guard<std::mutex> lock(m_ResultMutex);
	m_Results.push_back(result);
}
